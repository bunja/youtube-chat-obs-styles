#youtube-chat-obs-styles

This is a collection of css files that you can use with the `CLR Browser` or `Browser Source` OBS `source` media type.

It allows you to properly add youtube chat into any OBS stream.

##Getting Started

---

### Adding To your OBS scene

*Note: In older versions of OBS, the entry will be called CLR Browser, while in newer versions it will be called BrowserSource. The settings will be laid out differently, but can be applied all the same*

#### 1. Right click in your scene and select `BrowserSource`.

![Create BrowserSource Entry](tutorial_images/1.png)

#### 2. Name it whatever you like.

![Name The Entry](tutorial_images/2.png)

#### 3. Get the URL of the ***CURRENT*** stream chat you want by popping out the chat.

![Popout Chat](tutorial_images/3.png)

![Popout Chat URL](tutorial_images/4.png)

#### 4. Paste that url into the `BrowserSource` URL box.

![Paste The Url](tutorial_images/5.png)

#### 5. Set the chat dimensions (width, height) to whatever you prefer.

![Set Dimensions](tutorial_images/6.png)

#### 6. Copy the contents of streamable_chat.css into the css box

*Note: Feel free to first copy the contents into a text editor to modify it*

Direct link to streamable_chat.css: [Click Here](https://bitbucket.org/Frosthaven/youtube-chat-obs-styles/raw/master/streamable_chat.css)

![Copy CSS](tutorial_images/7.png)

![Paste CSS](tutorial_images/8.png)


#### 7. Press the OKAY button in OBS and you should now have a chat overlay!

![Done](tutorial_images/9.png)

*Note: Any time you change streams (such as running a scheduled stream instead of the 'Stream Now' option), you'll need to update the url to your current stream's popout url. You can right click your BrowserSource and select the properties option to edit the url, dimensions, etc. Any time youtube changes the way their website works, check back here for updates!*

#### 8. Further Customization

At this point, you can customize the CSS to your heart's content. If you know CSS rules, you can do
some pretty exotic things such as slide in/fade out animations (snippet located in [useful_snippets.css](https://bitbucket.org/Frosthaven/youtube-chat-obs-styles/raw/master/useful_snippets.css)):

![image preview](https://zippy.gfycat.com/GrimyWarmheartedHind.gif)



---

I provide this free for all to use and modify, with no small print exceptions.

If you want to support my projects, feel free to [drop a donation](http://paypal.me/frosthaven)!
